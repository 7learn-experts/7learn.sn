<?php
namespace App\Model;

class Like extends BaseModel {
    protected $table = 'likes';
    public $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function post(){
        return $this->belongsTo(Post::class,'post_id');
    }

}