<?php
namespace App\Model;

class Payment extends BaseModel {
    protected $table = 'payments';
    public $guarded = ['id'];

    public function ad()
    {
        return $this->belongsTo(Ads::class,'ads_id');
    }
}