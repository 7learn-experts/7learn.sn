<?php
namespace App\Model;

class Ads extends BaseModel {
    protected $table = 'ads';
    public $guarded = ['id'];

    public function payments(){
        return $this->hasMany(Payment::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }




}