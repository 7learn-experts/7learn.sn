<?php
namespace App\Model;

class User extends BaseModel {
    protected $table = 'users';
    public $guarded = ['id','role'];

    public function ads()
    {
        return $this->hasMany(Ads::class);
    }
    public function media()
    {
        return $this->hasMany(Media::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }


    public function followers()
    {
        return $this->belongsToMany(User::class,'follows','following_id','follower_id');
    }

    public function following()
    {
        return $this->belongsToMany(User::class,'follows','follower_id','following_id');
    }


}