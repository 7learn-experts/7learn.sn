<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
    public $timestamps = false;
    public $guarded = ['id'];
//    public $primaryKey = 'id';

}