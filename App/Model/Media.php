<?php
namespace App\Model;

class Media extends BaseModel {
    protected $table = 'media';
    public $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function post(){
        return $this->belongsTo(Post::class,'post_id');
    }



}