<?php
namespace App\Model;

class Hashtag extends BaseModel {
    protected $table = 'hashtags';
    public $guarded = ['id'];

    public function posts()
    {
        return $this->belongsToMany(Post::class,'post_hashtags','hashtag_id','post_id');
    }
}