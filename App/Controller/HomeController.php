<?php
namespace App\Controller;
use App\Repo\AdsRepo;
use App\Services\View\View;

class HomeController{
    public function index($request)
    {
        $adsRepo = new AdsRepo();
        $ad = $adsRepo->find(1);
        $pays = $ad->payments()->get();

        $data = [
            'post_count' => 3,
            'ad' => $ad,
            'pays' => $pays,
        ];

        View::load('home.index',$data,'home');
    }
}