<?php for ( $i= 0 ; $i<$post_count;$i++): ?>
<div class="panel panel-default">
    <div class="panel-heading"><h4>PostTitle</h4></div>
    <div class="panel-body">
        <a href="#">Post Body Here</a>
        <div class="clearfix"></div>
        <hr>
        <form>
            <div class="input-group">
                <div class="input-group-btn">
                    <button class="btn btn-default">+1</button>
                    <button class="btn btn-default"><i class="glyphicon glyphicon-heart"></i> 565</button>
                </div>
                <input class="form-control" placeholder="Add a comment.." type="text">
            </div>
        </form>
    </div>
</div>
<?php endfor; ?>
