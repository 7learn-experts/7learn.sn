<div class="flash-messages">
    <?php foreach ($flash_messages as $fm): ?>
        <div class="notice <?= \App\Services\Flash\FlashMessage::getCssClass($fm['type']) ?>"><?= $fm['msg'] ?></div>
    <?php endforeach; ?>
    <div class="close text-left" onclick="$('.flash-messages').fadeOut();">بستن</div>
</div>
