<?php
return [
    '/' => [
        'method' => 'get',
        'target' => 'HomeController@index',
        'middleware' => 'IEBlocker',
    ],
    // Home
    '/home' => [
        'method' => 'get',
        'target' => 'HomeController@index',
    ],
    // Post Routes
    '/post/save' => [
        'method' => 'post',
        'target' => 'PostController@save',
    ],
];