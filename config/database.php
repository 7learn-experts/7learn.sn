<?php
return [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => '7learn.sn',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
];