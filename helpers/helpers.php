<?php

function site_url($route){
    return (BASE_URL . ltrim($route,"/"));
}

function asset($path){
    return BASE_URL . "assets/$path";
}
