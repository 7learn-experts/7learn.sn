<?php

$content = "<?php
namespace App\Model;

class MODEL_NAME extends BaseModel {
    protected \$table = 'MODEL_TABLE_NAME';
    public \$guarded = ['id'];


}";


$models = [
    'Post' => 'posts',
    'ADS' => 'ads',
    'Comment' => 'comments',
    'Hashtag' => 'hashtags',
    'Like' => 'likes',
    'Media' => 'media',
    'Payment' => 'payments'
];

foreach ($models as $modelName=>$modelTable){
        $fileContent = str_replace(['MODEL_NAME','MODEL_TABLE_NAME'],[$modelName,$modelTable],$content);
        $fileName = "$modelName.php";
        if(file_exists($fileName)){
            file_put_contents($fileName,$fileContent);
        }
}


