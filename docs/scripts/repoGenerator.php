<?php

$content = "<?php
namespace App\Repo;
use App\Model\MODEL_NAME;

class MODEL_NAMERepo extends BaseRepo{
    public \$model = MODEL_NAME::class;


}
";


$models = [
    'Post' => 'posts',
    'ADS' => 'ads',
    'Comment' => 'comments',
    'Hashtag' => 'hashtags',
    'Like' => 'likes',
    'Media' => 'media',
    'Payment' => 'payments'
];

foreach ($models as $modelName=>$modelTable){
        $fileContent = str_replace('MODEL_NAME',$modelName,$content);
        $fileName = "{$modelName}Repo.php";
        if(!file_exists($fileName)){
            file_put_contents($fileName,$fileContent);
        }
}


